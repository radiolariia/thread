import React, { useState } from 'react';
import { Label, Icon, Modal, Button, Form } from 'semantic-ui-react';
import PropTypes from 'prop-types';

import styles from '../Post/styles.module.scss';

const EditPostModal = ({
  post,
  updatePost
}) => {
  const [isOpen, setIsOpen] = useState(false);
  const [updatedBody, setUpdatedBody] = useState('');
  // const [image, setImage] = useState(undefined);
  // const [isUploading, setIsUploading] = useState(false);

  const handleSubmit = postObj => {
    if (updatedBody && updatedBody !== postObj.body) {
      updatePost(post.id, updatedBody);
      setUpdatedBody('');
      setIsOpen(false);
    }
  };

  return (
    <Modal
      open={isOpen}
      onClose={() => setIsOpen(false)}
      basic
      size="small"
      centered
      trigger={(
        <Label
          style={{ float: 'right' }}
          onClick={() => setIsOpen(true)}
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
        >
          <Icon name="edit" />
        </Label>
      )}
    >
      <Modal.Content image>
        <Form onSubmit={() => handleSubmit(post)} style={{ width: '100%' }}>
          <Form.TextArea
            name="body"
            value={updatedBody}
            placeholder="What is the news?"
            onChange={ev => setUpdatedBody(ev.target.value)}
          />
          <Button floated="right" color="blue" type="submit">
            Update post
          </Button>
        </Form>
      </Modal.Content>
    </Modal>
  );
};

EditPostModal.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  updatePost: PropTypes.func.isRequired
};

export default EditPostModal;
