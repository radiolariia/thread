import React from 'react';
import PropTypes from 'prop-types';
import { Card, Image, Label, Icon } from 'semantic-ui-react';
import moment from 'moment';
import EditPostModal from '../EditPost/EditPost';
import styles from './styles.module.scss';

const Post = ({ post, likePost, dislikePost, toggleExpandedPost, sharePost, editPost, deletePost, loggedUserId }) => {
  const {
    id,
    image,
    body,
    user,
    activeReaction,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt
  } = post;

  const date = moment(createdAt).fromNow();
  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image.link} wrapped ui={false} />}
      <Card.Content>
        <Card.Meta>
          <span className="date">
            posted by
            {' '}
            {user.username}
            {' - '}
            {date}
          </span>
        </Card.Meta>
        <Card.Description>
          {body}
        </Card.Description>
      </Card.Content>
      <Card.Content extra>
        <Label 
          style={{ color: activeReaction === 'like' ? '#4183c4' : null }}
          basic 
          size="small" 
          as="a" 
          className={styles.toolbarBtn} 
          onClick={() => likePost(id)}
        >
          <Icon name="thumbs up" />
          {likeCount}
        </Label>
        <Label  
          style={{ color: activeReaction === 'dislike' ? '#4183c4' : null }}
          basic 
          size="small" 
          as="a" 
          className={styles.toolbarBtn} 
          onClick={() => dislikePost(id)}
        >
          <Icon name="thumbs down" />
          {dislikeCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleExpandedPost(id)}>
          <Icon name="comment" />
          {commentCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => sharePost(id)}>
          <Icon name="share alternate" />
        </Label>
        {loggedUserId === user.id && (
          <>
            <Label
              style={{ float: 'right' }}
              basic 
              size="small" 
              as="a" 
              className={styles.toolbarBtn} 
              onClick={() => deletePost(id)}
            >
              <Icon name="trash" />
            </Label>
            <EditPostModal
              updatePost={editPost}
              post={post}
              id={id}
            >
              <Label
                basic
                size="small"
                as="a"
                className={styles.toolbarBtn}
              >
                <Icon name="trash" />
              </Label>
            </EditPostModal>
          </>
        )}
      </Card.Content>
    </Card>
  );
};

Post.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  editPost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired,
  loggedUserId: PropTypes.string
};

Post.defaultProps = {
  loggedUserId: undefined
};

export default Post;
