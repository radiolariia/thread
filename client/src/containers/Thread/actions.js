import * as postService from 'src/services/postService';
import * as commentService from 'src/services/commentService';
import {
  ADD_POST,
  LOAD_MORE_POSTS,
  SET_ALL_POSTS,
  SET_EXPANDED_POST
} from './actionTypes';

const setPostsAction = posts => ({
  type: SET_ALL_POSTS,
  posts
});

const addMorePostsAction = posts => ({
  type: LOAD_MORE_POSTS,
  posts
});

const addPostAction = post => ({
  type: ADD_POST,
  post
});

const setExpandedPostAction = post => ({
  type: SET_EXPANDED_POST,
  post
});

export const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPostsAction(posts));
};

export const loadMorePosts = filter => async (dispatch, getRootState) => {
  const { posts: { posts } } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts
    .filter(post => !(posts && posts.some(loadedPost => post.id === loadedPost.id)));
  dispatch(addMorePostsAction(filteredPosts));
};

export const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPostAction(post));
};

export const addPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPostAction(newPost));
};

export const deletePost = postId => async (dispatch, getRootState) => {
  const { id } = await postService.deletePost(postId);

  const { posts: { posts } } = getRootState();
  const refreshedPosts = posts.filter(post => (post.id !== id));
  dispatch(setPostsAction(refreshedPosts));
};

export const editPost = (postId, data) => async (dispatch, getRootState) => {
  await postService.updatePost(postId, data);
  
  const mapEditedPosts = post => ({
    ...post,
    body: data
  });

  const { posts: { posts } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapEditedPosts(post)));
  
  dispatch(setPostsAction(updated));
};

export const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPostAction(post));
};

const validateOneReact = (like, isLiked, createdAt, updatedAt, reaction) => {
  let likeDiff = 0;
  let dislikeDiff = 0;
  let activeReaction = reaction;

  switch (isLiked) {
    case true:
      likeDiff = 1;
      if (createdAt !== updatedAt) {
        dislikeDiff = -1;
      }
      activeReaction = 'like';
      break;
    case false:
      dislikeDiff = 1;
      if (createdAt !== updatedAt) {
        likeDiff = -1;
      }
      activeReaction = 'dislike';
      break;
    default:
      if (like) {
        likeDiff = -1;
      } else {
        dislikeDiff = -1;
      }
      activeReaction = '';
  }
  return {
    likeDiff,
    dislikeDiff,
    reaction: activeReaction
  };
};

const processPostReactions = async (postId, isLikeAction, activeReaction, dispatch, getRootState) => {
  const { isLike, createdAt, updatedAt } = await postService.reactOnPost(postId, isLikeAction);

  const { likeDiff, dislikeDiff, reaction } = validateOneReact(
    isLikeAction,
    isLike,
    createdAt,
    updatedAt,
    activeReaction
  );
  const mapReactions = post => ({
    ...post,
    // stored state to get access to its previous value
    activeReaction: reaction,
    likeCount: Number(post.likeCount) + likeDiff, // diff is taken from the current closure
    dislikeCount: Number(post.dislikeCount) + dislikeDiff
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapReactions(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapReactions(expandedPost)));
  }
};

export const likePost = (postId, activeReaction, isLike = true) => async (dispatch, getRootState) => {
  processPostReactions(postId, isLike, activeReaction, dispatch, getRootState);
};

export const dislikePost = (postId, activeReaction, isLike = false) => async (dispatch, getRootState) => {
  processPostReactions(postId, isLike, activeReaction, dispatch, getRootState);
};

export const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId
    ? post
    : mapComments(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};
